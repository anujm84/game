const path = require('path');
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const webpackCommon = require('./webpack.common.js');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = webpackMerge(webpackCommon, {
    plugins: [
        new UglifyJsPlugin({
            compress: true,
            warnings: false,
            toplevel: false
        })

    ],
    output: {
        path: path.resolve(process.cwd(), 'dist'),
        filename: '[name].bundle.js'
    }
});

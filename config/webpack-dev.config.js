const webpackMerge = require('webpack-merge');
const webpackCommon = require('./webpack.common.js');
const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = webpackMerge(webpackCommon, {
    plugins: [
                
    ],
    output: {
        path: path.resolve(process.cwd(), 'dev'),
        filename: '[name].bundle.dev.js'
    }
});

const path = require('path');
const env = process.env.NODE_ENV || "dev";
const helpers = require('helpers');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')

var commonconfig = {
    entry: {
        app: './src/main',
        //polyfill: './src/polyfill',
        //vendor: './src/vendor'
    },
    resolve: {
        extensions: [
            '.js',
            '.ts'
        ]
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: [
                    'raw-loader'
                ],
                exclude: [/node_modules/]
            },
            {
                test: /\.js$/,
                loader: [
                    'babel-loader'
                ],
                exclude: [/node_modules/]
            },
            {
                test: /\.(css|scss|sass|stylus)$/,
                loader: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(png|jpe?g)$/,
                loader: 'file-loader?name=assets/[name].[hash].[ext]'
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin(),
        new CopyWebpackPlugin([
            {
                from: './src/assets/',
                to: 'assets'
            }
        ])
        
    ]
};

module.exports = commonconfig;